package com.kostrych.App;

import java.io.Serializable;

public class Droid implements Serializable {
    private String nick;
    private int power;
    private int armor;
    private transient boolean isImmortal;

    public Droid(String nick, int power, int armor, boolean isImmortal) {
        this.nick = nick;
        this.power = power;
        this.armor = armor;
        this.isImmortal = isImmortal;
    }

    public String getNick() {
        return nick;
    }

    public int getPower() {
        return power;
    }

    public int getArmor() {
        return armor;
    }

    public boolean isImmortal() {
        return isImmortal;
    }
}
