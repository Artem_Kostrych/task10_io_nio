package com.kostrych.App;

import com.kostrych.App.Droid;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ship {

    private List<Droid> droids;

    public Ship() {
        droids = new ArrayList<Droid>();
    }

    public void addDroidToShip(int N) {
        Scanner scanner = new Scanner(System.in);
        int i = 0;
        while (i < N) {
            System.out.println("Value for " + (i + 1) + " droid:");
            System.out.println("Name of droid");
            String name = scanner.next();

            System.out.println("Power of droid");
            int power = scanner.nextInt();

            System.out.println("Armor of droid");
            int armor = scanner.nextInt();

            System.out.println("Is Immortal?");
            boolean isImmortal = scanner.nextBoolean();

            droids.add(new Droid(name, power, armor, isImmortal));

            i++;
        }
    }

    public void writeToFile() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("ship.txt"))) {
            oos.writeObject(droids);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("ship.txt"))) {
            ArrayList<Droid> ship = (ArrayList<Droid>) ois.readObject();
            for (Droid d : ship) {
                System.out.println("Nick - " + d.getNick()
                        + "\nPower - " + d.getPower()
                        + "\nArmor - " + d.getArmor()
                        + "\nImmortal? - " + d.isImmortal() + "\n");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

